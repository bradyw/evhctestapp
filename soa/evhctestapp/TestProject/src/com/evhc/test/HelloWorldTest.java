package com.evhc.test;

import com.opitzconsulting.soa.testing.AbstractSoaTest;


import com.opitzconsulting.soa.testing.util.SoapUtil;

import org.junit.Before;
import org.junit.Test;

public class HelloWorldTest extends AbstractSoaTest {
    @Before
    public void init() {           
        declareXpathNS("soap", "http://schemas.xmlsoap.org/soap/envelope/");
        declareXpathNS("bpel", "http://xmlns.oracle.com/evhctestapp/Project1/BPELProcess2");
    }
    
    @Test
    public void doTest() {
        // arrange
        String request = SoapUtil.getInstance().soapEnvelope(SoapUtil.SoapVersion.SOAP11, readClasspathFile("input.xml"));
        
        // act
        // don't hard code versions, this could be used with a release branch
        String response = invokeCompositeService("Project1", System.getProperty("composite.version"), "bpelprocess2_client_ep", request);
        
        // assert
        System.out.println("got response "+response);
        assertXpathEvaluatesTo("soap:Envelope/soap:Body/bpel:processResponse/bpel:result/text()", "sync hello hello 123", response);
    }
}
